from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest, time

class NewVisitorTest(LiveServerTestCase):
    

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_django_processing_web_page_set_up_correct_and_can_register(self):
        self.browser.get(self.live_server_url)

        ###homepage check###
        self.assertIn('', self.browser.title)
        time.sleep(6)
        self.browser.find_element_by_id('b').click()

        


if __name__ == '__main__':
    unittest.main(warnings='ignore')
