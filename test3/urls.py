from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    url(r'^$', 'testapps.views.home', name='home'),
    url(r'^ask/$', 'testapps.views.ask', name='ask'),
    url(r'^answer/$', 'testapps.views.answer', name='answer'),
    url(r'^stat/$', 'testapps.views.stat', name='stat'),
    url(r'^choose/$', 'testapps.views.choose', name='choose'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
]
