from django.shortcuts import render, redirect
from django.http import HttpResponse
from testapps.models import Question, Answer, Choice
from django.utils import timezone

maxchoice = 0
def calmaxchoice():
    global maxchoice
    maxchoice = 0
    for q in Question.objects.all():
        qnum = Choice.objects.filter(question=q.question).count()
        maxchoice = (qnum > maxchoice) and qnum or maxchoice

class statistic:
    def __init__(self, answer, num):
        self.answer = answer
        self.num = num

class queststat:
    def __init__(self, question, num):
        self.question = question
        self.num = num
        self.average = []

def home(request):
    return render(request, 'home.html')

def ask(request):
    if request.method == "POST":
        ques = request.POST["question"]
        try:
            same = Question.objects.get("ques")
            render(request, 'ask.html', {'fail': 'ques'})
        except: pass
        Question.objects.create(question=ques)
        choice = []
        numi = 0
        try:
            while 1:
                request.POST["choice"+str(numi+1)]
                numi += 1
        except: pass
        for i in range(numi):
            ch = request.POST.get("choice"+str(i+1), "")
            if ch != "":
                Choice.objects.create(question=ques,choice=ch)
    return render(request, 'ask.html')

def answer(request):
    calmaxchoice()
    if request.method == "POST":
        if request.POST.get("answer", "") != "":
            Answer.objects.create(question=request.POST["question"],answer=request.POST["answer"])
            return redirect('/choose')
        else :
            try: 
                ques = Question.objects.get(question=request.POST["choose"])
            except: ques = Question.objects.get(question=request.POST["quest"])
            
            choice = Choice.objects.filter(question=ques.question)
            return render(request, 'answer.html', {
                       'quest': ques.question,
                       'choices': choice,
                   })
    return redirect('/choose')

def stat(request):
    calmaxchoice()
    ques = Question.objects.all()
    averages = []
    total = [0]*(maxchoice+1)
    #stat
    for q in ques:
        averages.append(queststat(q.question, Answer.objects.filter(question=q.question).count()))
        choices = Choice.objects.filter(question=q.question)
        i = 0
        for ch in choices:
            num = Answer.objects.filter(question=q.question,answer=ch.choice).count()
            averages[-1].average.append(statistic(ch.choice, num))
            total[i] += num
            i += 1
        for j in range(maxchoice-i):
            averages[-1].average.append(statistic("N", "A"))
    total[maxchoice] = Answer.objects.count()
    #recent
    day, month = [], []
    print("eikeie"+str(timezone.now().date()) + "eieiei")
    for answer in Answer.objects.all():
        if answer.date == timezone.now().date():
            print("eiei"+answer.date+"haha")
            day.append(answer)
        if answer.date.split('-')[0] == str(timezone.now().year):
            if answer.date.split('-')[1] == str(timezone.now().month).rjust(2, '0'):
                month.append(answer)

    return render(request, 'stat.html', {
                     'averages': averages,
                     'totals': total,
                     'max': range(maxchoice),
                     'day': day,
                     'month': month,
                 })

def choose(request):
    return render(request, 'choose.html', {'questions': Question.objects.all()})
