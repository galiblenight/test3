import pytz
from django.db import models
from django.utils import timezone

class Choice(models.Model):
    date = models.TextField(default=timezone.now().date())
    question = models.TextField(default='')
    choice = models.TextField(default='')

class Question(models.Model):
    date = models.TextField(default=timezone.now().date())
    question = models.TextField(default='')

class Answer(models.Model):
    date = models.TextField(default=timezone.now().date())
    question = models.TextField(default='')
    answer = models.TextField(default=0)
